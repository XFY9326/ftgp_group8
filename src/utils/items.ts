import { GoodsItem } from "@/api/data";
import GoodsCover from "@/assets/images/goods.png";

export const GOODS_ITEMS: GoodsItem[] = [
    {
        name: "Item 1",
        item: "item_1",
        cover: GoodsCover,
    },
    {
        name: "Item 2",
        item: "item_2",
        cover: GoodsCover,
    },
    {
        name: "Item 3",
        item: "item_3",
        cover: GoodsCover,
    },
    {
        name: "Item 4",
        item: "item_4",
        cover: GoodsCover,
    },
    {
        name: "Item 5",
        item: "item_5",
        cover: GoodsCover,
    },
    {
        name: "Item 6",
        item: "item_6",
        cover: GoodsCover,
    },
    {
        name: "Item 7",
        item: "item_7",
        cover: GoodsCover,
    },
    {
        name: "Item 8",
        item: "item_8",
        cover: GoodsCover,
    },
    {
        name: "Item 9",
        item: "item_9",
        cover: GoodsCover,
    },
    {
        name: "Item 10",
        item: "item_10",
        cover: GoodsCover,
    },
];

export function findGoodsItem(item: string | undefined) {
    if (item !== undefined) return GOODS_ITEMS.find((i) => i.item === item);
}
