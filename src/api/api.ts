import { getEthersProvider } from "@/api/metamask";
import { Platform__factory } from "@/api/types";

const CONTRACT_ADDRESS = "";

export async function connectPlatformContract() {
    const provider = await getEthersProvider();
    if (provider) {
        return Platform__factory.connect(CONTRACT_ADDRESS, provider);
    } else {
        return null;
    }
}
